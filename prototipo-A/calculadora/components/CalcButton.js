
import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native'; //importamos componentes

export default class CalcButton extends React.Component {

  static defaultProps = { 
    onPress: function() { }, 
    title: "",
    color: "white",
    backgroundColor: "black",
    radius: 40,

    style: { },
  }


  render() { //características de los botones
    var r = this.props.radius; 
    var w = this.props.radius * 2; 
    var h = this.props.radius * 2;
    var bc = this.props.backgroundColor;

    return (
      <TouchableOpacity onPress={this.props.onPress} 
        style={[styles.container, { width: w, height: h, borderRadius: r, backgroundColor: bc }, { ...this.props.style }]}>
        <Text style={[styles.text, { color: this.props.color }]}>{this.props.title}</Text>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: { alignItems: "center", justifyContent: "center", margin: 6,},
  text: { fontSize: 55, fontWeight: "bold", },
});