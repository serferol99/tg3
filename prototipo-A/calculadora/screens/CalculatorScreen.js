
require("../lib/swisscalc.lib.format.js");
require("../lib/swisscalc.lib.operator.js");
require("../lib/swisscalc.lib.operatorCache.js");
require("../lib/swisscalc.lib.shuntingYard.js");
require("../lib/swisscalc.display.numericDisplay.js");
require("../lib/swisscalc.display.memoryDisplay.js");
require("../lib/swisscalc.calc.calculator.js");

import React from 'react';
import { StyleSheet, Dimensions, PanResponder, View, Text } from 'react-native'; //importamos los componentes
import { CalcDisplay, CalcButton } from '../components'; //importamos las clases

export default class CalculatorScreen extends React.Component {
  //constructor
  constructor(props) {
    super(props);
    this.state = {
      display: "0",
      orientation: "portrait",    
    }
    
    // inicializamos la calculadora
    this.oc = global.swisscalc.lib.operatorCache;
    this.calc = new global.swisscalc.calc.calculator();

    Dimensions.addEventListener('change', () => {
      const { width, height } = Dimensions.get("window");
      var orientation = (width > height) ? "landscape" : "portrait";
      this.setState({ orientation: orientation });
    });

    
    this.panResponder = PanResponder.create({
      onStartShouldSetPanResponder: (evt, gestureState) => true,
      onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
      onMoveShouldSetPanResponder: (evt, gestureState) => true,
      onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,
      onPanResponderMove: (evt, gestureState) => { },
      onPanResponderRelease: (evt, gestureState) => {
        if (Math.abs(gestureState.dx) >= 50) {
          this.onBackspacePress();
        }
      },
    })
  }
  //método para almacenar los números que introducimos
  pulsarNumero = (num) => {
    this.calc.addDigit(num);
    this.setState({ display: this.calc.getMainDisplay() });
  }
  //método que realiza la operacion de %
  pulsarPorcentaje = (operador) => {
    this.calc.addUnaryOperator(operador);
    this.setState({ display: this.calc.getMainDisplay() });
  }
  //método que almacena el operador que introducimos
  pulsarOperador = (operador) => {
    this.calc.addBinaryOperator(operador);
    this.setState({ display: this.calc.getMainDisplay() });
  }
  //método que realiza el =
  pulsarIgual = () => {
    this.calc.equalsPressed();
    this.setState({ display: this.calc.getMainDisplay() });
  }
  //método para limpiar
  borrar = () => {
    this.calc.clear();
    this.setState({ display: this.calc.getMainDisplay() });
  }
  //método para la operación de negación
  negacion = () => {
    this.calc.negate();
    this.setState({ display: this.calc.getMainDisplay() });
  }

  onBackspacePress = () => {
    this.calc.backspace();
    this.setState({ display: this.calc.getMainDisplay() });
  }

  //renderizamos la calculadora con sus botones y sus respectivas posiciones
  renderPortrait() {
    return (
      <View style={styles.container}>
        <View style={{flex:1, justifyContent: "flex-end"}} {...this.panResponder.panHandlers}>
          <CalcDisplay display={this.state.display} />
        </View>

        <View>
          <View style={{flexDirection: "row", justifyContent: "space-between",}}>                
            <CalcButton onPress={() => { this.pulsarPorcentaje(this.oc.PercentOperator) }} title="%" color="#f8bbd0" backgroundColor="#7986cb" />
            <CalcButton onPress={this.negacion} title="+/-" color="#f8bbd0" backgroundColor="#7986cb" />
            <CalcButton onPress={this.borrar} title="C" color="#f8bbd0" backgroundColor="#7986cb" />
            <CalcButton onPress={() => { this.pulsarOperador(this.oc.DivisionOperator) }} title="/" color="#ffab91" backgroundColor="#009688" />
          </View>

          <View style={{flexDirection: "row", justifyContent: "space-between",}}>
            <CalcButton onPress={() => { this.pulsarNumero("7") }} title="7" color="white" backgroundColor="#4fc3f7" />
            <CalcButton onPress={() => { this.pulsarNumero("8") }} title="8" color="white" backgroundColor="#4fc3f7" />
            <CalcButton onPress={() => { this.pulsarNumero("9") }} title="9" color="white" backgroundColor="#4fc3f7" />
            <CalcButton onPress={() => { this.pulsarOperador(this.oc.MultiplicationOperator) }} title="x" color="#ffab91" backgroundColor="#009688" />
          </View>

          <View style={{flexDirection: "row", justifyContent: "space-between",}}>
            <CalcButton onPress={() => { this.pulsarNumero("4") }} title="4" color="white" backgroundColor="#4fc3f7" />
            <CalcButton onPress={() => { this.pulsarNumero("5") }} title="5" color="white" backgroundColor="#4fc3f7" />
            <CalcButton onPress={() => { this.pulsarNumero("6") }} title="6" color="white" backgroundColor="#4fc3f7" />
            <CalcButton onPress={() => { this.pulsarOperador(this.oc.SubtractionOperator) }} title="-" color="#ffab91" backgroundColor="#009688" />
          </View>

          <View style={{flexDirection: "row", justifyContent: "space-between",}}>
            <CalcButton onPress={() => { this.pulsarNumero("1") }} title="1" color="white" backgroundColor="#4fc3f7" />
            <CalcButton onPress={() => { this.pulsarNumero("2") }} title="2" color="white" backgroundColor="#4fc3f7" />
            <CalcButton onPress={() => { this.pulsarNumero("3") }} title="3" color="white" backgroundColor="#4fc3f7" />
            <CalcButton onPress={() => { this.pulsarOperador(this.oc.AdditionOperator) }} title="+" color="#ffab91" backgroundColor="#009688" />
          </View>

          <View style={{flexDirection: "row", justifyContent: "space-between",}}>
            <CalcButton onPress={() => { this.pulsarNumero("0") }} title="0" color="white" backgroundColor="#4fc3f7" style={{flex:2}} />
            <CalcButton onPress={() => { this.pulsarNumero(".") }} title="." color="white" backgroundColor="#4fc3f7" />
            <CalcButton onPress={this.pulsarIgual} title="=" color="#ffab91" backgroundColor="#009688" />
          </View>
        </View>

      </View>
    );
  }

  renderLandscape() {
    return (
      <View>
        <Text>Landscape</Text>
      </View>
    )
  }

  render() {
    var view = (this.state.orientation == "portrait")
      ? this.renderPortrait()
      : this.renderLandscape();

    return (
      <View style={{flex:1}}>
        {view}
      </View>
    )
  }

}

const styles = StyleSheet.create({
  container: { flex: 2, paddingVertical: 5, backgroundColor: "#263238" },
})